const stats = [1,2,3,4];

class Character {
    Name;
    HP;
    STR;
    DEX;
    CON;
    INT;

    constructor(Name) {
        this.Name = Name;
    
        this.STR = this.SelectRandom();
        this.CON = this.SelectRandom();
        this.INT = this.SelectRandom();
        this.DEX = this.SelectRandom();
        this.HP = this.CON*10;
    }

    SelectRandom = () => {
        let x = Math.floor(Math.random()*stats.length);
        let y = stats[x];
        delete stats[x];
        return y;
    }

    GetSTR = () => {return this.STR;}
    GetDEX = () => {return this.DEX;}
    GetINT = () => {return this.INT;}
    GetCON = () => {return this.CON;}
    GetHP = () => {return this.HP;}
    GetName = () => {return this.Name;}
    
    ToString = () => {
        return "Name: " + this.Name +"\n"+ "HP: "+ this.HP + "\n"+"STR: " + this.STR + "\n" + "DEX: " + this.DEX + "\n" + "INT: " + this.INT + "\n" + "CON: " + this.CON;
    }
}


    let Character1 = new Character("Me");
    console.log(Character1.ToString());


module.exports = {Character1};
