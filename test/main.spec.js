const expect = require('chai').expect;
const { Character1 } = require("../main.js");
let stats = [];

describe("Character", () => {
    it("Characters information", () => {
        expect(Character1.Name).to.equal("Me");
        
        
    });
    it("All stats are unique", () => {
        expect(IsUnique(Character1.STR)).to.equal(true);
        expect(IsUnique(Character1.INT)).to.equal(true);
        expect(IsUnique(Character1.DEX)).to.equal(true);
        expect(IsUnique(Character1.CON)).to.equal(true);
    });
    it("HP is calcculated correctly" , () => {
        expect(Character1.HP).to.equal(Character1.CON*10);
    });

});

IsUnique = (stat) => {
    stats.push(Character1.STR);
    stats.push(Character1.INT);
    stats.push(Character1.DEX);
    stats.push(Character1.CON);
    let x = 0;
    for(let i = 0; i<stats.length-1; i++) {
        if(stat == stats) {
            x++;
        }
    }
    if(x>1) {
        return false;
    } else {
        return true;
    }
}
